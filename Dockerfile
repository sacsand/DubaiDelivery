FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# COPY package.json yarn.lock ./
# For npm@5 or later, copy package-lock.json as well
# COPY package.json package-lock.json ./

ENV NPM_CONFIG_LOGLEVEL warn

# Install all dependencies of the current project.
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn install

# Copy all local files into the image.
COPY . .

# Install and configure `serve`.
RUN yarn build
CMD yarn serve
EXPOSE 8000