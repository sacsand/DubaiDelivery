import { makeExecutableSchema } from 'graphql-tools';
import gql from 'graphql-tag';

import resolvers from './resolvers';

const typeDefs = gql`
  type User {
    shortID: String
    name: String
    email: String
    isSuper: String
    role: String
    phoneNumber: String
    active: String
  }

  type PageInfo {
    total: Int
    page: Int
    pages: Int
  }

  type Users {
    edges: [User]
    pageInfo: PageInfo
  }

  input UserInput {
    shortID: String
    name: String
    email: String
    phoneNumber: String
    isSuper: String
  }

  type Mutation {
    updateUser(input: UserInput): User
  }

  type Query {
    user(shortID: String!): User
    users(shortID: String, name: String): Users
  }
`;

export default makeExecutableSchema({
  typeDefs,
  resolvers
});

// get user and users
// query getUser {
//   user(shortID: "rknZO6f17") {
//     firstName
//     email
//   }
// }

// query getUsers {
//   users(page: 1, limit: 10, email: "c4c5nf3@gmail.com") {
//     edges {
//       firstName
//       email
//     }
//     pageInfo {
//       total
//       page
//       pages
//     }
//   }
// }
