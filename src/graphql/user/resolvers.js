import mongoose from 'mongoose';
import { User, Item } from '../../models';
import { isSuperAdmin } from '../../lib/helper';

export default {
  Query: {
    users: async (root, { page, limit, shortID, name, phoneNumber }, context) =>
      new Promise(async (resolve, reject) => {
        const query = {};

        const options = {
          sort: { createdAt: 1 },
          lean: true,
          page,
          limit
        };

        // // check
        // if (nic) {
        //   query.nic = new RegExp(nic, 'i');
        // }

        // if (gender) {
        //   query.gender = gender;
        // }

        // if (civilStatus) {
        //   query.civilStatus = civilStatus;
        // }

        // if (shortID) {
        //   query.shortID = new RegExp(shortID, 'i');
        // }

        // if (firstName) {
        //   query.firstName = new RegExp(firstName, 'i');
        // }

        // if (lastName) {
        //   query.lastName = new RegExp(lastName, 'i');
        // }

        // if (email) {
        //   query.email = new RegExp(email, 'i');
        // }

        // if (phoneNumber) {
        //   query.phoneNumber = new RegExp(phoneNumber, 'i');
        // }

        User.paginate(query, options)
          .then(async result => {
            const { docs, total, pages } = result;

            // const edges = await Promise.all(
            //   docs.map(async user => (

            //   ))
            // );

            const edges = docs;

            resolve({
              edges,
              pageInfo: {
                total,
                page: result.page,
                pages
              }
            });
          })
          .catch(error => {
            reject(error);
          });
      }),
    user: (root, { shortID }, context) =>
      new Promise(async (resolve, reject) => {
        const query = { shortID };

        try {
          User.findOne(query)
            .lean()
            .exec((error, doc) => {
              if (error) {
                reject(error);
              }

              resolve(doc);
            });
        } catch (error) {
          reject(error);
        }
      })
  },
  // mutation
  Mutation: {
    updateUser: (root, { input }, context) =>
      new Promise(async (resolve, reject) => {
        console.log(111, input);
        let newUser = null;
        const user = input;
        const { shortID } = input;
        await delete user.shortID;
        try {
          newUser = await User.findOneAndUpdate({ shortID }, user, { new: true }).lean();
        } catch (error) {
          reject(error);
          throw error;
        }

        resolve(newUser);
      })
  }
};
