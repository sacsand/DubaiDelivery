import { mergeSchemas } from 'graphql-tools';

import UserSchema from './user/schema';
import PackageSchema from './package/schema';

export default mergeSchemas({
  schemas: [UserSchema, PackageSchema]
});
