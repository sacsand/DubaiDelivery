import { makeExecutableSchema } from 'graphql-tools';
import GraphQLJSON from 'graphql-type-json';
import gql from 'graphql-tag';
import mongoosePaginate from 'mongoose-paginate';

import resolvers from './resolvers';

const typeDefs = gql`
  scalar JSON

  type Package {
    shortID: String
    barcodeID: String
    thumbURL: String
    note: String
    data: String
    shippingAddress: String
    status: String
    customerPhone: String
    isReadyToDeliver: String
    isDiliveryConfirmed: String
    adminNote: String
    diliveryDate: String
    logs: JSON
  }

  type PageInfo {
    total: Int
    page: Int
    pages: Int
  }

  type Packages {
    edges: [Package]
    pageInfo: PageInfo
  }

  input PackageInput {
    shortID: String
    barcodeID: String
    thumbURL: String
    note: String
    data: String
    shippingAddress: String
    status: String
    customerPhone: String
    isReadyToDeliver: String
    isDiliveryConfirmed: String
    adminNote: String
    diliveryDate: String
  }

  type Mutation {
    createPackage(input: PackageInput): Package
    updatePackage(input: PackageInput): Package
  }

  type Query {
    Package(shortID: String!): Package
    Packages(
      page: Int
      limit: Int
      barcodeID: String
      isReadyToDeliver: String
      isDiliveryConfirmed: String
      status: String
    ): Packages
  }
`;

export default makeExecutableSchema({
  typeDefs,
  resolvers
});

// query getPackages {
//  Packages(page: 1, limit: 10,meta:"new meat",title:"new",isDeleted:"false") {
//     edges {
//       shortID
//       title
//       description
//       meta
//       isDeleted
//       createdAt
//       updatedAt

//     }
//     pageInfo {
//       total
//       page
//       pages
//     }
//   }
// }
