import mongoose from 'mongoose';
import GraphQLJSON from 'graphql-type-json';
import { User, Package } from '../../models';

export default {
  JSON: GraphQLJSON,
  Query: {
    Packages: async (root, { page, limit, barcodeID }, context) =>
      new Promise(async (resolve, reject) => {
        const query = {};

        const options = {
          sort: { createdAt: 1 },
          lean: true,
          page,
          limit
        };

        // if (meta) {
        //   query.meta = meta;
        // }

        // if (title) {
        //   query.title = new RegExp(title, 'i');
        // }

        // if (isDeleted) {
        //   query.isDeleted = isDeleted;
        // }

        Package.paginate(query, options)
          .then(async result => {
            const { docs, total, pages } = result;

            const edges = docs;

            resolve({
              edges,
              pageInfo: {
                total,
                page: result.page,
                pages
              }
            });
          })
          .catch(error => {
            reject(error);
          });
      }),
    Package: (root, { shortID }, context) =>
      new Promise(async (resolve, reject) => {
        // const query = { shortID };

        Package.findOne({ shortID })
          .lean()
          .exec((error, doc) => {
            if (error) {
              reject(error);
            }

            resolve(doc);
          });
      })
  },
  Mutation: {
    createPackage: (root, { input }, context) =>
      new Promise(async (resolve, reject) => {
        console.log(111, input);
        let newPackage = null;

        try {
          newPackage = new Package(input);
        } catch (error) {
          reject(error);
        }

        console.log(222, newPackage);

        try {
          await newPackage.validate();
        } catch (error) {
          console.log(error);
          throw error;
        }

        try {
          await newPackage.save();
        } catch (error) {
          reject(error);
          console.log(error);
          throw error;
        }

        resolve(newPackage);
      }),

    updatePackage: (root, { input }, context) =>
      new Promise(async (resolve, reject) => {
        console.log(111, input);
        const packagex = input;
        const { shortID } = input;
        let newPackage = null;
        await delete packagex.shortID;
        try {
          newPackage = await Package.findOneAndUpdate({ shortID }, packagex, { new: true }).lean();
        } catch (error) {
          reject(error);
          throw error;
        }

        resolve(newPackage);
      })
  }
};
