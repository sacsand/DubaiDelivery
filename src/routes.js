import express from 'express';
import { createUser, getUsers, fireUpUser } from './modules/user';

const router = express.Router();

const routeHandler = ({ path, callback, method }) => {
  router.route(path)[method](async (req, res, next) => {
    try {
      await callback(req, res, next);
    } catch (error) {
      // rollbar.error(error);

      next(error);
    }
  });
};

// User routes
routeHandler({
  path: '/signup',
  callback: createUser,
  method: 'post'
});

// User routes
routeHandler({
  path: '/fireupuser',
  callback: fireUpUser,
  method: 'post'
});

routeHandler({
  path: '/user',
  callback: getUsers,
  method: 'get'
});

module.exports = router;
