import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import filter from 'content-filter';
import cors from 'cors';
import dotenv from 'dotenv';
import mongoose from 'mongoose';

// import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import { createFirebaseAuth } from 'express-firebase-auth';

import { express as voyagerMiddleware } from 'graphql-voyager/middleware';

import firebase from './lib/firebase';
import routes from './routes';
import schema from './graphql/schema';
import { DB_CONFIG } from './config';

// Setup environment config
dotenv.config();

// Initiate the app
const app = express();

// Connect to the database
mongoose.connect(DB_CONFIG.uri, DB_CONFIG.options);
mongoose.Promise = global.Promise;

app.use('/voyager', voyagerMiddleware({ endpointUrl: '/grapvoyager' }));

// Apply the middleware
app.use(filter());

app.use(helmet.xssFilter());
app.use(helmet.hidePoweredBy());
app.use(helmet.noSniff());
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(logger('dev'));

// Add the firebase authenticatior
const ignoredUrls = [];

if (process.env.NODE_ENV === 'dev') {
  ignoredUrls.push('/graphiql');
  ignoredUrls.push('/user');
  ignoredUrls.push('/signup');
  ignoredUrls.push('/fireupuser');
  ignoredUrls.push('/voyager');
  ignoredUrls.push('/graphvoyager');
}

const firebaseAuth = createFirebaseAuth({ firebase, ignoredUrls });
// app.use(firebaseAuth);

app.use('/', routes);

// Add graphql
app.use(
  '/graphql',
  graphqlExpress((req, res) => ({
    schema
    // context: { user: res.locals.user }
  }))
);
app.use(
  '/grapvoyager',
  graphqlExpress((req, res) => ({
    schema
    // context: { user: res.locals.user }
  }))
);

app.use(
  '/graphiql',
  graphiqlExpress({
    endpointURL: '/graphql'
  })
);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({ error: 'Something failed!' });
});

// Setup the ip and port
app.set('port', process.env.APP_PORT || 8080);
app.set('ip', process.env.APP_IP || '127.0.0.1');

// Start the app
app.listen(app.get('port'), () => {
  /* eslint-disable no-console */
  console.log('***************************************************************');
  console.log(`Server started on ${Date(Date.now())}`);
  console.log(`Server is listening on port ${app.get('port')}`);
  console.log('***************************************************************');
});

module.exports = app;
