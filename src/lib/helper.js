import { User } from '../models';

export const isSuperAdmin = async (query) => {
  let user = null;
  let state = false;
  try {
    user = await User.findOne(query).lean();

    console.log(user);

    if (!user) {
      console.log('user not found');
    }

    if (user.isSuper === true) {
      state = true;
      // throw new Error({ message: 'User does not have permission to view' });
    }
  } catch (error) {
    throw error;
  }

  return state;
};
