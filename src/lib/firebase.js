import firebase from 'firebase-admin';

let serviceAccount;

/* eslint-disable import/no-unresolved */
if (process.env.NODE_ENV === 'staging') {
  serviceAccount = require('../../staging-firebase-config.json');
} else if (process.env.NODE_ENV === 'production') {
  serviceAccount = require('../../firebase-config.json');
} else {
  serviceAccount = require('../../dev-firebase-config.json');
}
/* eslint-enable import/no-unresolved */

// Initialize firebase
firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: `https://${process.env.FIREBASE_DATABASE_NAME}.firebaseio.com`
});

export const firebaseCreateUser = async ({ user }) => {
  try {
    const firebaseUser = await firebase.auth().createUser({
      email: user.email,
      emailVerified: false,
      password: user.password,
      disabled: false
    });

    return firebaseUser;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const firebaseDeleteUser = async uid => {
  try {
    await firebase.auth().deleteUser(uid);
  } catch (error) {
    // TODO: notify developer
  }
};

export const firebaseVerifyUser = async uid => {
  try {
    await firebase.auth().updateUser(uid, {
      emailVerified: true
    });
  } catch (error) {
    throw error;
  }
};

export const firebaseUpdateUser = async ({ uid, userData }) => {
  try {
    await firebase.auth().updateUser(uid, userData);
  } catch (error) {
    throw error;
  }
};

const getUser = async uid => {
  try {
    const user = await firebase.auth().getUser(uid);

    return user;
  } catch (error) {
    throw error;
  }
};

export { getUser };
export default firebase;
