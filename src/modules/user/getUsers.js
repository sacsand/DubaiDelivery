import { User } from '../../models';

export const getUsers = async (req, res) => {
  const { query } = req.query;

  const search = JSON.parse(query);
  const { page, limit } = search;
  await delete search.page;
  await delete search.limit;

  const users = await User.paginate(search, { page, limit });

  res.send({
    users
  });
};
