import { User } from '../../models';
import { firebaseDeleteUser, getUser } from '../../lib/firebase';

// import sendVerificationEmail from './helpers/sendVerificationEmail';
// import sendVerificationSMS from './helpers/sendVerificationSMS';

export const fireUpUser = async (req, res) => {
  const { user } = req.body;

  try {
    const isuser = await User.findOne({ email: user.email }).lean();
    console.log(23, isuser);
    if (isuser) {
      res.status(200);
      res.send({ user });
    }
  } catch (error) {
    res.status(500);
    res.send({ error: 'user find error' });

    throw error;
  }

  console.log(222, user);

  // 1) Validate user
  const newUser = new User(user);

  try {
    await newUser.validate();
  } catch (error) {
    res.status(500);
    res.send({ error: 'User validation failed!' });

    throw error;
  }

  // // 2) check user
  // try {
  //   const firebaseUser = await getUser(user.firebaseid);

  //   newUser.firebaseUID = firebaseUser.uid;
  // } catch (error) {
  //   res.status(500);
  //   res.send({ error: 'User not found ' });
  //   throw error;
  // }

  // 3) Save the user

  try {
    await newUser.save();
  } catch (error) {
    res.status(500);
    res.send({ error: 'DB error!' });
    throw error;
  }

  res.send({
    user: newUser
  });
};
