import { User } from '../../models';

import { firebaseUpdateUser } from '../../lib/firebase';

export const updateUser = async (req, res) => {
  const { user: userData } = req.body;

  const firebaseUser = res.locals.user;
  const firebaseUID = firebaseUser.user_id;

  let user;

  // If email is changed
  try {
    if (firebaseUser.email !== userData.email) {
      await firebaseUpdateUser({
        uid: firebaseUID,
        userData: {
          email: userData.email
        }
      });
    }
  } catch (error) {
    if (error.code === 'auth/email-already-exists') {
      res.status(500);
      res.send({ error: 'User already exists!', code: 'email-already-exists' });
    } else {
      res.status(500);
      res.send({ error: 'User creation failed!' });
    }

    throw error;
  }

  // Update the user
  try {
    user = await User.findOneAndUpdate({ firebaseUID }, userData, { new: true }).lean();
  } catch (error) {
    res.status(500);
    res.send({ error: 'DB error!' });

    throw error;
  }

  res.send({
    user: {
      shortID: user.shortID,
      firebaseUID: user.firebaseUID,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      phoneNumber: user.phoneNumber
    }
  });
};
