import generator from 'generate-password';

import { User } from '../../models';
import { firebaseCreateUser, firebaseDeleteUser } from '../../lib/firebase';

// import sendVerificationEmail from './helpers/sendVerificationEmail';
// import sendVerificationSMS from './helpers/sendVerificationSMS';

export const createUser = async (req, res) => {
  const { user } = req.body;

  console.log(222, user);

  // 1) Validate user
  const newUser = new User(user);

  try {
    await newUser.validate();
  } catch (error) {
    res.status(500);
    res.send({ error: 'User validation failed!' });

    throw error;
  }

  // 2) Create firebase user
  try {
    const firebaseUser = await firebaseCreateUser({
      user
    });

    newUser.firebaseUID = firebaseUser.uid;
  } catch (error) {
    if (error.code === 'auth/email-already-exists') {
      res.status(500);
      res.send({ error: 'User already exists!', code: 'email-already-exists' });
    } else {
      res.status(500);
      res.send({ error: 'User creation failed!' });
    }

    throw error;
  }

  // 3) Save the user

  try {
    await newUser.save();
  } catch (error) {
    await firebaseDeleteUser(newUser.firebaseUID);

    res.status(500);
    res.send({ error: 'DB error!' });
    throw error;
  }

  res.send({
    newUserID: newUser.shortID
  });
};
