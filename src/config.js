import dotenv from 'dotenv';

dotenv.config();

const DB_CONFIG = {
  uri: process.env.MONGODB_URI,
  options: {
    useMongoClient: true
  }
};

export { DB_CONFIG };
