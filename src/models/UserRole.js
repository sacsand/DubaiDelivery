import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserRoleSchema = new Schema(
  {
    userID: {
      type: String,
      required: true,
      trim: true
    },
    roleID: {
      type: String,
      required: true,
      trim: true
    }
  },
  {
    timestamps: true
  }
);

const UserRole = mongoose.model('UserRole', UserRoleSchema);

export { UserRole };
