import mongoose from 'mongoose';

const userCompanySchema = new mongoose.Schema();

export const UserCompany = mongoose.model('UserCompany', userCompanySchema, 'usercompanies');
