import mongoose from 'mongoose';
import shortid from 'shortid';

const Schema = mongoose.Schema;

const CompanySchema = new Schema(
  {
    shortID: {
      type: String,
      trim: true,
      unique: true
    },
    name: {
      type: String,
      required: true,
      trim: true
    },
    street: {
      type: String,
      required: true,
      trim: true
    },
    city: {
      type: String,
      required: true,
      trim: true
    },
    state: {
      type: String,
      required: true,
      trim: true
    },
    countryCode: {
      type: String,
      required: true,
      trim: true
    },
    telephone: {
      type: String,
      required: true,
      trim: true
    },
    web: {
      type: String,
      trim: true
    },
    subscriptionId: {
      type: Schema.Types.ObjectId,
      ref: 'Subcsription',
      trim: true
    },
    active: {
      type: Boolean,
      default: true
    }
  },
  {
    timestamps: true
  }
);

CompanySchema.pre('save', async function(next) {
  if (this.shortID) {
    next(); // skip it
    return; // stop this function from running
  }

  this.shortID = shortid.generate();

  next();
});

const Company = mongoose.model('Company', CompanySchema);

export { Company };
