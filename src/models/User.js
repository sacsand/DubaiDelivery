import mongoose from 'mongoose';
import mongooseHiddenFN from 'mongoose-hidden';
import isEmail from 'validator/lib/isEmail';
import shortid from 'shortid';
import mongoosePaginate from 'mongoose-paginate';

const mongooseHidden = mongooseHiddenFN();

const { Schema } = mongoose;

const UserSchema = new Schema(
  {
    shortID: {
      type: String,
      trim: true,
      unique: true
    },
    firebaseUID: {
      type: String,
      trim: true,
      unique: true,
      sparse: true
    },
    name: {
      type: String,
      required: true,
      trim: true
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      trim: true,
      validate: [{ validator: value => isEmail(value), msg: 'Invalid email.' }]
    },
    phoneNumber: {
      type: String,
      trim: true,
      sparse: true
    },
    active: {
      type: Boolean,
      default: true
    },
    address: {
      type: String,
      trim: true
    },
    isSuper: {
      type: Boolean,
      default: false
    },
    role: {
      type: String,
      trim: true
    },
    photoUrl: {
      type: String,
      trim: true
    }
  },
  {
    timestamps: true
  }
);

UserSchema.plugin(mongoosePaginate);

UserSchema.pre('save', async function(next) {
  if (this.shortID) {
    next(); // skip it
    return; // stop this function from running
  }

  this.shortID = shortid.generate();

  next();
});

UserSchema.plugin(mongooseHidden, {
  hidden: {
    _id: true,
    __v: true,
    phoneVerificationCode: true,
    verificationCode: true,
    active: true
  }
});

const User = mongoose.model('User', UserSchema);

export { User };
