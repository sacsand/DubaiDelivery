import mongoose from 'mongoose';
import mongooseHiddenFN from 'mongoose-hidden';
import shortid from 'shortid';
import mongoosePaginate from 'mongoose-paginate';

const mongooseHidden = mongooseHiddenFN();

const Schema = mongoose.Schema;

const PackageSchema = new Schema(
  {
    shortID: {
      type: String,
      trim: true,
      unique: true
    },
    barcodeID: {
      type: String,
      trim: true
    },
    thumbURL: {
      type: String,
      trim: true
    },
    note: {
      type: String,
      trim: true
    },
    data: {
      type: Schema.Types.Mixed
    },
    shippingAddress: {
      type: String,
      trim: true
    },
    assignedTo: {
      type: Schema.Types.ObjectId,
      trim: true
    },
    status: {
      type: String,
      trim: true
    },
    updatedAdress: {
      type: String,
      trim: true
    },
    isReadyToDeliver: {
      type: Boolean,
      default: false
    },
    isDiliveryConfirmed: {
      type: Boolean,
      default: false
    },
    adminNote: {
      type: String,
      trim: true
    },
    diliveryDate: {
      type: Date,
      trim: true
    },
    customerPhone: {
      type: String,
      trim: true
    },
    logs: {
      type: Schema.Types.Mixed
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      trim: true
    }
  },
  {
    timestamps: true
  }
);

PackageSchema.plugin(mongoosePaginate);

PackageSchema.pre('save', async function(next) {
  if (this.shortID) {
    next(); // skip it
    return; // stop this function from running
  }

  this.shortID = shortid.generate();

  next();
});

PackageSchema.plugin(mongooseHidden, {
  hidden: {
    _id: true,
    __v: true
  }
});

const Package = mongoose.model('Package', PackageSchema);

export { Package };
